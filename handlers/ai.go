package handlers

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

func NewAiHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		gatewayUrl := os.Getenv("AI_GATEWAY_URL")
		gatewayPath := strings.Replace(r.URL.Path, "/ai/", "", 1)
		newUrl := gatewayUrl + "/" + gatewayPath

		httpClient := http.Client{}
		req, err := http.NewRequest(r.Method, newUrl, r.Body)
		if err != nil {
			fmt.Println("err:", err)
		}
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("X-Gitlab-Authentication-Type", "oidc")
		req.Header.Add("Authorization", "Bearer "+r.Header.Get("X-Gitlab-Token"))
		fmt.Println(req)
		resp, err := httpClient.Do(req)
		if err != nil {
			w.WriteHeader(503)
			fmt.Println("Failed to request", newUrl)
			return
		}

		w.WriteHeader(resp.StatusCode)
		if _, err := io.Copy(w, resp.Body); err != nil {
			w.WriteHeader(503)
			fmt.Println("Failed streaming response from", newUrl)
		}
	})
}
